package com.example.osmaluca.alfiquiz;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.osmaluca.alfiquiz.Beans.UsuarioPersona;
import com.example.osmaluca.alfiquiz.Tabs.TabPersonaDatos;
import com.example.osmaluca.alfiquiz.Tabs.TabPersonaEncuesta;
import com.example.osmaluca.alfiquiz.Tabs.TabPersonaPromocion;

import org.json.JSONArray;

public class LoginPersonaActivity extends AppCompatActivity {

    private TextView lblNombrePersona,lblPuntos,lblCorreo;
    private Button btnPromocion,btnEncuesta,btnFragments;
    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_persona);

        lblNombrePersona = findViewById(R.id.lblNombrePersona);
        lblCorreo = findViewById(R.id.lblCorreo);
        lblPuntos = findViewById(R.id.lblPuntos);
//        btnPromocion = findViewById(R.id.btnListarPromocion);
//        btnEncuesta = findViewById(R.id.btnListarEncuesta);
//        btnFragments = findViewById(R.id.btnFragments);

//        btnEncuesta.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(LoginPersonaActivity.this,ListarPersonaEncuestaActivity.class);
//                startActivity(intent);
//            }
//        });
//
//        btnPromocion.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(LoginPersonaActivity.this,ListarPersonaPromocionActivity.class);
//                startActivity(intent);
//            }
//        });
//
//        btnFragments.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(LoginPersonaActivity.this,FragmentsActivity.class);
//                startActivity(intent);
//            }
//        });

        UsuarioPersona usu = Control.getInstance().usuarioPersona;

        lblNombrePersona.setText("Nombre: "+usu.getNombre()+" "+usu.getApellido_paterno()+" "+usu.getApellido_materno());
        lblCorreo.setText("Correo: "+usu.getCorreo());
        lblPuntos.setText("Puntos: "+usu.getPuntaje_bonificacion());

    }

    public void listarEncuestaPersona(){
        requestQueue = Volley.newRequestQueue(this);
        String url = "http://192.168.1.7/api/encuesta/listarEncuestaPersona/";
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Control.getInstance().listaEncuesta = response;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(jsonArrayRequest);
    }

    public void listarPromocionPersona(){
        requestQueue = Volley.newRequestQueue(this);
        String url = "http://192.168.1.7/api/promocion/listarPromocionGeneral/";
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Control.getInstance().listaPromocion = response;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(jsonArrayRequest);
    }
}
