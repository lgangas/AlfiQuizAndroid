package com.example.osmaluca.alfiquiz.Beans;

/**
 * Created by osmaluca on 01/07/2018.
 */

public class UsuarioEmpresa {
    private String codigo_usuario_empresa;
    private String ruc;
    private String nombre;
    private String correo;
    private String clave;
    private int estado_registro;

    public String getCodigo_usuario_empresa() {
        return codigo_usuario_empresa;
    }

    public void setCodigo_usuario_empresa(String codigo_usuario_empresa) {
        this.codigo_usuario_empresa = codigo_usuario_empresa;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public int getEstado_registro() {
        return estado_registro;
    }

    public void setEstado_registro(int estado_registro) {
        this.estado_registro = estado_registro;
    }
}
