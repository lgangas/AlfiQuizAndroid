package com.example.osmaluca.alfiquiz;

import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.example.osmaluca.alfiquiz.Beans.UsuarioPersona;
import com.example.osmaluca.alfiquiz.Tabs.TabPersonaDatos;
import com.example.osmaluca.alfiquiz.Tabs.TabPersonaEncuesta;
import com.example.osmaluca.alfiquiz.Tabs.TabPersonaPromocion;

public class FragmentsActivity extends AppCompatActivity implements
        TabPersonaDatos.OnFragmentInteractionListener,
        TabPersonaEncuesta.OnFragmentInteractionListener,
        TabPersonaPromocion.OnFragmentInteractionListener{


    private Toolbar myToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragments);

        myToolbar = findViewById(R.id.my_toolbar);

        setSupportActionBar(myToolbar);

        TabLayout tabLayout = findViewById(R.id.TabPersona);
        tabLayout.addTab(tabLayout.newTab().setText("Encuesta"));
        tabLayout.addTab(tabLayout.newTab().setText("Promocion"));
        tabLayout.addTab(tabLayout.newTab().setText("Usuario"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = findViewById(R.id.PagerPersona);
        final TabPersonaAdapter adapter = new TabPersonaAdapter(getSupportFragmentManager(),tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
