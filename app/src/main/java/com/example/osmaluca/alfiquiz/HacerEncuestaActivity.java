package com.example.osmaluca.alfiquiz;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HacerEncuestaActivity extends AppCompatActivity {

    private LinearLayout layoutEncuesta;
    private RequestQueue requestQueue;
    private TextView lblNumeroPregunta, lblPregunta;
    private Button btnAtras,btnSiguiente;
    private static int numero;
    private int alternativa;
    private JSONObject jsonObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hacer_encuesta);

        layoutEncuesta = findViewById(R.id.layoutHacerEncuesta);
        btnAtras = findViewById(R.id.btnAtras);
        btnSiguiente = findViewById(R.id.btnSiguiente);
        lblNumeroPregunta = findViewById(R.id.lblNumeroPregunta);
        lblPregunta = findViewById(R.id.lblPregunta);

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(numero == 1){
                    AlertDialog.Builder dialogo1 = new AlertDialog.Builder(view.getContext());
                    dialogo1.setTitle("Importante");
                    dialogo1.setMessage("¿Desea salir de la encuesta?");
                    dialogo1.setCancelable(true);
                    dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                            try {
                                Intent intent = new Intent(HacerEncuestaActivity.this,FragmentsActivity.class);
                                startActivity(intent);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                            dialogo1.cancel();
                        }
                    });
                    dialogo1.show();
                } else {
                    Control.getInstance().numeroPregunta = Control.getInstance().numeroPregunta - 2;
                    Intent intent = new Intent(HacerEncuestaActivity.this,HacerEncuestaActivity.class);
                    startActivity(intent);
                }
            }
        });

        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                String respuesta = "";
                if(alternativa == 0){
                    RadioGroup radioGroup = findViewById(Integer.parseInt("10"));
                    respuesta = ""+radioGroup.getCheckedRadioButtonId();
                } else {
                    EditText editText = findViewById(Integer.parseInt("10"));
                    respuesta = editText.getText().toString();
                }

                if(!respuesta.isEmpty() && !respuesta.equals("-1")){
                    JSONArray jRespuesta ;
                    jRespuesta = Control.getInstance().preguntaRegistrada;
                    int numeroPreguntas  = Control.getInstance().preguntaRegistrada.length();
                    jsonObject = Control.getInstance().preguntaActual;

                    try {
                        jsonObject.put("respuesta",respuesta);
                        if(jRespuesta.isNull(numero-1)){
                            Control.getInstance().preguntaRegistrada.put(jsonObject);
                        } else {
                            Control.getInstance().preguntaRegistrada.put(numero-1,jsonObject);
                        }

                        int preguntasTotales = Control.getInstance().encuestaActual.getInt("cantidad_pregunta");

                        if(numero < preguntasTotales){
                            Intent intent = new Intent(HacerEncuestaActivity.this,HacerEncuestaActivity.class);
                            startActivity(intent);
                        }else {
                            AlertDialog.Builder dialogo1 = new AlertDialog.Builder(view.getContext());
                            dialogo1.setTitle("Importante");
                            dialogo1.setMessage("¿Completo la encuesta?");
                            dialogo1.setCancelable(true);
                            dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogo1, int id) {
                                    try {
//                                        String er = Control.getInstance().preguntaRegistrada.toString();
//                                        Toast.makeText(HacerEncuestaActivity.this, er, Toast.LENGTH_SHORT).show();
                                        hacerEncuesta(view);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogo1, int id) {
                                    dialogo1.cancel();
                                }
                            });
                            dialogo1.show();
                        }



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(HacerEncuestaActivity.this, "Escoja o escriba una respuesta.", Toast.LENGTH_LONG).show();
                }
            }
        });
        obtenerPreguntar();
    }
    public void hacerEncuesta(final View view){
        try {
            requestQueue = Volley.newRequestQueue(this);
            String url = "http://192.168.1.7/api/encuesta/hacerEncuesta/";
            JSONObject jRequest = new JSONObject();
            jRequest.put("codigo_usuario_persona", Control.getInstance().usuarioPersona.getCodigo_usuario_persona());
            jRequest.put("puntos", Control.getInstance().encuestaActual.getInt("bonificacion"));
            jRequest.put("respuestas", Control.getInstance().preguntaRegistrada);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(JsonObjectRequest.Method.POST, url, jRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        boolean status = response.getBoolean("status");
                        String title = "";
                        if(status){
                            int misPuntos = Control.getInstance().usuarioPersona.getPuntaje_bonificacion() + Control.getInstance().encuestaActual.getInt("bonificacion");
                            Control.getInstance().usuarioPersona.setPuntaje_bonificacion(misPuntos);
                            title = "Felicidades";
                        }else {
                            title = "Error";
                        }
                        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(view.getContext());
                        dialogo1.setTitle(title);
                        dialogo1.setMessage(response.getString("msg"));
                        dialogo1.setCancelable(true);
                        dialogo1.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogo1, int id) {
                                try {
                                    Intent intent = new Intent(HacerEncuestaActivity.this,FragmentsActivity.class);
                                    startActivity(intent);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        dialogo1.show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });

            requestQueue.add(jsonObjectRequest);
        }catch (Exception  ex){

        }
    }

    public void obtenerPreguntar() {
        try{
            requestQueue = Volley.newRequestQueue(this);
            String url = "http://192.168.1.7/api/encuesta/obtenerPreguntar/";
            Control.getInstance().numeroPregunta = Control.getInstance().numeroPregunta +1;
            JSONObject jRequest = new JSONObject();
            jRequest.put("encuesta",Control.getInstance().encuestaActual.getInt("codigo_encuesta"));
            jRequest.put("orden",Control.getInstance().numeroPregunta);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, jRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Control.getInstance().preguntaActual = response;
                    pintarVista(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            requestQueue.add(jsonObjectRequest);
        }catch (Exception e){

        }
    }

    public void pintarVista(final JSONObject jsonObject){
        try {
            if(jsonObject.length() > 0){
                numero = jsonObject.getInt("numero_orden");
                alternativa = jsonObject.getInt("alternativa");
                pintarElementos(jsonObject);
            } else {

            }
        }catch (Exception ex){
            ex.getStackTrace();
        }
    }

    public void pintarElementos(JSONObject jsonObject) {
        try{
            String pregunta = jsonObject.getString("pregunta");

            lblNumeroPregunta.setText("Pregunta "+numero+":");
            lblPregunta.setText(pregunta);

            if(alternativa == 0){
                JSONArray jsonArray = jsonObject.getJSONArray("listarAlternativas");
                int sizeArray = jsonArray.length();
                RadioGroup radioGroup = new RadioGroup(this);
                radioGroup.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                radioGroup.setId(Integer.parseInt("10"));

                for(int i = 0; i < sizeArray; i++){
                    RadioButton rbAlternativa = new RadioButton(this);
                    rbAlternativa.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    rbAlternativa.setText(jsonArray.getJSONObject(i).getString("texto"));
                    rbAlternativa.setId(i+1);
                    radioGroup.addView(rbAlternativa);
                }
                layoutEncuesta.addView(radioGroup);

                if(!Control.getInstance().preguntaRegistrada.isNull(numero-1)){
                    String key = Control.getInstance().preguntaRegistrada.getJSONObject(numero-1).getString("respuesta");
                    RadioButton rbAlternativaCheck = findViewById(Integer.parseInt(key));
                    rbAlternativaCheck.setChecked(true);
                }

            } else {
                EditText editText = new EditText(this);
                editText.setId(Integer.parseInt("10"));
                editText.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                editText.setEms(20);
                editText.setInputType(InputType.TYPE_TEXT_FLAG_IME_MULTI_LINE);
                layoutEncuesta.addView(editText);
                if(!Control.getInstance().preguntaRegistrada.isNull(numero-1)){
                    String respuesta = Control.getInstance().preguntaRegistrada.getJSONObject(numero-1).getString("respuesta");
                    EditText editTextCheck = findViewById(Integer.parseInt("10"));
                    editTextCheck.setText(respuesta);
                }
            }
        }catch (Exception ex){
            ex.getStackTrace();
        }
    }
}
