package com.example.osmaluca.alfiquiz;

import com.example.osmaluca.alfiquiz.Beans.UsuarioEmpresa;
import com.example.osmaluca.alfiquiz.Beans.UsuarioPersona;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by osmaluca on 01/07/2018.
 */

public class Control {
    private static Control control = new Control();
    private Control (){}
    public static Control getInstance(){return control;}

    public UsuarioEmpresa usuarioEmpresa;
    public UsuarioPersona usuarioPersona;
    public JSONObject encuestaActual;
    public JSONObject preguntaActual;
    public JSONArray preguntaRegistrada = new JSONArray();
    public JSONArray listaEncuesta;
    public JSONArray listaPromocion;
    public int numeroPregunta;
}
