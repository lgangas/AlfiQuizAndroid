package com.example.osmaluca.alfiquiz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.osmaluca.alfiquiz.Beans.UsuarioEmpresa;
import com.example.osmaluca.alfiquiz.Beans.UsuarioPersona;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private EditText usuario,clave;
    private Button ingresar;
    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        usuario = findViewById(R.id.txtUsuario);
        clave = findViewById(R.id.txtClave);
        ingresar = findViewById(R.id.btnIngresar);

        ingresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ingresar(view);
            }
        });

    }

    public void ingresar(View view){
        requestQueue = Volley.newRequestQueue(view.getContext());
        String url = "http://192.168.1.7/api/usuario/ingresar";
        JSONObject jRequest = getRequest();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jRequest, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ingresarUsuario(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(jsonObjectRequest);
    }

    public JSONObject getRequest(){
        JSONObject jRequest = new JSONObject();
        try {
            jRequest.put("correo",usuario.getText());
            jRequest.put("clave",clave.getText());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jRequest;
    }

    public void ingresarUsuario(JSONObject response){
        try {
            final String tipoUsuario = response.getString("tipo");
            String codUsuario = response.getString("codigo");
            requestQueue = Volley.newRequestQueue(this);
            String url = "";
            if(tipoUsuario.equals("1")){
                url = "http://192.168.1.7/api/usuario/ingresarpersona/"+codUsuario;
            } else {
                url = "http://192.168.1.7/api/usuario/ingresarempresa/"+codUsuario;
            }
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    redirigirLogin(response,tipoUsuario);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });

            requestQueue.add(jsonObjectRequest);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void redirigirLogin(JSONObject response, String tipoUsuario){
        try {
            if(tipoUsuario.equals("1")){
                UsuarioPersona usuario = new UsuarioPersona();
                usuario.setCodigo_usuario_persona(response.getString("codigo_usuario_persona"));
                usuario.setDni(response.getString("dni"));
                usuario.setNombre(response.getString("nombre"));
                usuario.setApellido_paterno(response.getString("apellido_paterno"));
                usuario.setApellido_materno(response.getString("apellido_materno"));
                usuario.setCorreo(response.getString("correo"));
                usuario.setClave(response.getString("clave"));
                usuario.setPuntaje_bonificacion(response.getInt("puntaje_bonificacion"));
                usuario.setEstado_registro(response.getInt("estado_registro"));

                Control.getInstance().usuarioPersona = usuario;
                Intent intent = new Intent(this,FragmentsActivity.class);
                startActivity(intent);
            } else {
                UsuarioEmpresa usuario = new UsuarioEmpresa();
                usuario.setCodigo_usuario_empresa(response.getString("codigo_usuario_empresa"));
                usuario.setRuc(response.getString("ruc"));
                usuario.setNombre(response.getString("nombre"));
                usuario.setCorreo(response.getString("correo"));
                usuario.setClave(response.getString("clave"));
                usuario.setEstado_registro(response.getInt("estado_registro"));

                Control.getInstance().usuarioEmpresa = usuario;
                Intent intent = new Intent(this,FragmentsActivity.class);
                startActivity(intent);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}























