package com.example.osmaluca.alfiquiz;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.osmaluca.alfiquiz.Tabs.TabPersonaDatos;
import com.example.osmaluca.alfiquiz.Tabs.TabPersonaEncuesta;
import com.example.osmaluca.alfiquiz.Tabs.TabPersonaPromocion;

import org.json.JSONArray;

/**
 * Created by osmaluca on 01/07/2018.
 */

public class TabPersonaAdapter extends FragmentStatePagerAdapter {

    private int numeroTabs;

    public TabPersonaAdapter(FragmentManager fm, int numeroTabs) {
        super(fm);
        this.numeroTabs = numeroTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0:
                TabPersonaEncuesta tabPersonaEncuesta = new TabPersonaEncuesta();
                return tabPersonaEncuesta;
            case 1:
                TabPersonaPromocion tabPersonaPromocion = new TabPersonaPromocion();
                return tabPersonaPromocion;
            case 2:
                TabPersonaDatos tabPersonaDatos = new TabPersonaDatos();
                return tabPersonaDatos;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numeroTabs;
    }
}
