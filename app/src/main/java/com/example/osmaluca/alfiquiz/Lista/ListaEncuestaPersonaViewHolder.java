package com.example.osmaluca.alfiquiz.Lista;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.osmaluca.alfiquiz.R;

public class ListaEncuestaPersonaViewHolder extends RecyclerView.ViewHolder {

    private TextView lblEncuesta,lblEmpresa,lblPuntos,lblToken;


    public ListaEncuestaPersonaViewHolder(View itemView) {
        super(itemView);

        lblEncuesta = itemView.findViewById(R.id.lblEncuesta);
        lblEmpresa = itemView.findViewById(R.id.lblEmpresa);
        lblPuntos = itemView.findViewById(R.id.lblPuntos);
        lblToken = itemView.findViewById(R.id.lblToken);
    }

    public TextView getLblEncuesta() {
        return lblEncuesta;
    }

    public TextView getLblEmpresa() {
        return lblEmpresa;
    }

    public TextView getLblPuntos() {
        return lblPuntos;
    }

    public TextView getLblToken() {
        return lblToken;
    }
}
