package com.example.osmaluca.alfiquiz.Beans;

/**
 * Created by osmaluca on 01/07/2018.
 */

public class UsuarioPersona {
    private String codigo_usuario_persona;
    private String dni;
    private String nombre;
    private String apellido_paterno;
    private String apellido_materno;
    private String correo;
    private String clave;
    private int puntaje_bonificacion;
    private int estado_registro;

    public String getCodigo_usuario_persona() {
        return codigo_usuario_persona;
    }

    public void setCodigo_usuario_persona(String codigo_usuario_persona) {
        this.codigo_usuario_persona = codigo_usuario_persona;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido_paterno() {
        return apellido_paterno;
    }

    public void setApellido_paterno(String apellido_paterno) {
        this.apellido_paterno = apellido_paterno;
    }

    public String getApellido_materno() {
        return apellido_materno;
    }

    public void setApellido_materno(String apellido_materno) {
        this.apellido_materno = apellido_materno;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public int getPuntaje_bonificacion() {
        return puntaje_bonificacion;
    }

    public void setPuntaje_bonificacion(int puntaje_bonificacion) {
        this.puntaje_bonificacion = puntaje_bonificacion;
    }

    public int getEstado_registro() {
        return estado_registro;
    }

    public void setEstado_registro(int estado_registro) {
        this.estado_registro = estado_registro;
    }
}
