package com.example.osmaluca.alfiquiz.Lista;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.osmaluca.alfiquiz.Control;
import com.example.osmaluca.alfiquiz.FragmentsActivity;
import com.example.osmaluca.alfiquiz.LoginPersonaActivity;
import com.example.osmaluca.alfiquiz.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ListaPromocionPersonaAdapter extends RecyclerView.Adapter<ListaPromocionPersonaViewHolder> {

    private JSONArray jsonArray;

    public ListaPromocionPersonaAdapter(JSONArray jsonArray){
        this.jsonArray = jsonArray;
    }

    @NonNull
    @Override
    public ListaPromocionPersonaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_lista_promocion_persona,parent,false);
        return new ListaPromocionPersonaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListaPromocionPersonaViewHolder holder, int position) {
        try {
            final JSONObject obj = jsonArray.getJSONObject(position);
            final int puntosPromo = obj.getInt("bonificacion_pago");
            holder.getLblTitulo().setText(obj.getString("titulo"));
            holder.getLblEmpresa().setText(obj.getString("empresa"));
            holder.getLblPuntos().setText(obj.getString("bonificacion_pago"));
            String uri = "http://192.168.1.7/img/"+obj.getString("imagen");
            Glide
                    .with(holder.itemView.getContext())
                    .load(uri)
                    .into(holder.getIvPromocion());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    int puntosPersona = Control.getInstance().usuarioPersona.getPuntaje_bonificacion();

                    if(puntosPersona >= puntosPromo){
                        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(view.getContext());
                        dialogo1.setTitle("Importante");
                        dialogo1.setMessage("¿Desea adquirir esta promocion?");
                        dialogo1.setCancelable(true);
                        dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogo1, int id) {
                                try {
                                    comprarPromocion(view,obj);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogo1, int id) {
                                dialogo1.cancel();
                            }
                        });
                        dialogo1.show();
                    } else {
                        Toast.makeText(view.getContext(), "No tienes suficientes puntos para adquirir esta promocion.", Toast.LENGTH_LONG).show();
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void comprarPromocion(final View view, final JSONObject jsonObject) throws JSONException {
        JSONObject jRequest = new JSONObject();
        jRequest.put("usuario",Control.getInstance().usuarioPersona.getCodigo_usuario_persona());
        jRequest.put("promocion",jsonObject.getInt("codigo_promocion"));
        jRequest.put("puntos",jsonObject.getInt("bonificacion_pago"));
        RequestQueue requestQueue = Volley.newRequestQueue(view.getContext());
        String url = "http://192.168.1.7/api/promocion/comprarPromocion/";
        JsonObjectRequest stringRequest = new JsonObjectRequest(JsonObjectRequest.Method.POST, url,jRequest, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Toast.makeText(view.getContext(), response.getString("msg"), Toast.LENGTH_LONG).show();
                    int puntosPromo = jsonObject.getInt("bonificacion_pago");
                    int puntosPersona = Control.getInstance().usuarioPersona.getPuntaje_bonificacion();
                    Control.getInstance().usuarioPersona.setPuntaje_bonificacion(puntosPersona - puntosPromo);
                    Intent intent = new Intent(view.getContext(), FragmentsActivity.class);
                    view.getContext().startActivity(intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(stringRequest);
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }
}
