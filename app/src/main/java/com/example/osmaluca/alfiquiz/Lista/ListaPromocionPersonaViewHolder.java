package com.example.osmaluca.alfiquiz.Lista;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.osmaluca.alfiquiz.R;

public class ListaPromocionPersonaViewHolder extends RecyclerView.ViewHolder {

    private TextView lblTitulo,lblPuntos,lblEmpresa;
    private ImageView ivPromocion;

    public ListaPromocionPersonaViewHolder(View itemView) {
        super(itemView);

        lblTitulo = itemView.findViewById(R.id.lblTitulo);
        lblPuntos = itemView.findViewById(R.id.lblPuntos);
        lblEmpresa = itemView.findViewById(R.id.lblEmpresa);
        ivPromocion = itemView.findViewById(R.id.ivPromocion);
    }

    public TextView getLblTitulo() {
        return lblTitulo;
    }

    public TextView getLblPuntos() {
        return lblPuntos;
    }

    public TextView getLblEmpresa() {
        return lblEmpresa;
    }

    public ImageView getIvPromocion() {
        return ivPromocion;
    }
}
