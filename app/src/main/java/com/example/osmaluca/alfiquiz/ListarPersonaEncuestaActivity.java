package com.example.osmaluca.alfiquiz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.osmaluca.alfiquiz.Lista.ListaEncuestaPersonaAdapter;

import org.json.JSONArray;

public class ListarPersonaEncuestaActivity extends AppCompatActivity {

    private RecyclerView rvListaEncuesta;
    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_persona_encuesta);

        rvListaEncuesta = findViewById(R.id.rvEncuestaPersona);
        listarEncuestaPersona();
    }

    public void listarEncuestaPersona(){
        requestQueue = Volley.newRequestQueue(this);
        String url = "http://192.168.1.7/api/encuesta/listarEncuestaPersona/";
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                ListaEncuestaPersonaAdapter adapter = new ListaEncuestaPersonaAdapter(response);
                rvListaEncuesta.setLayoutManager(new LinearLayoutManager(ListarPersonaEncuestaActivity.this));
                rvListaEncuesta.setAdapter(adapter);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(jsonArrayRequest);
    }
}
