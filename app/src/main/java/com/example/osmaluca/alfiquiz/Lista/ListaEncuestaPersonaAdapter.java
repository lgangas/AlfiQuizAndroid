package com.example.osmaluca.alfiquiz.Lista;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.osmaluca.alfiquiz.Control;
import com.example.osmaluca.alfiquiz.HacerEncuestaActivity;
import com.example.osmaluca.alfiquiz.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ListaEncuestaPersonaAdapter extends RecyclerView.Adapter<ListaEncuestaPersonaViewHolder> {

    private JSONArray jsonArray;

    public ListaEncuestaPersonaAdapter(JSONArray jsonArray){
        this.jsonArray = jsonArray;
    }

    @NonNull
    @Override
    public ListaEncuestaPersonaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_lista_encuesta_persona,parent,false);
        return new ListaEncuestaPersonaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListaEncuestaPersonaViewHolder holder, int position) {
        try {
            final JSONObject obj = jsonArray.getJSONObject(position);
            holder.getLblEncuesta().setText(obj.getString("encuesta"));
            holder.getLblEmpresa().setText(obj.getString("empresa"));
            holder.getLblPuntos().setText(obj.getString("bonificacion"));
            holder.getLblToken().setText(obj.getString("token"));
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Control.getInstance().encuestaActual = obj;
                    Control.getInstance().numeroPregunta = 0;
                    Intent intent = new Intent(view.getContext(), HacerEncuestaActivity.class);
                    view.getContext().startActivity(intent);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }
}
