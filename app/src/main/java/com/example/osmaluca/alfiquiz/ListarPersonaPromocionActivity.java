package com.example.osmaluca.alfiquiz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.osmaluca.alfiquiz.Lista.ListaPromocionPersonaAdapter;

import org.json.JSONArray;

public class ListarPersonaPromocionActivity extends AppCompatActivity {

    private RecyclerView rvListarPromocion;
    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_persona_promocion);

        rvListarPromocion = findViewById(R.id.rvPromocionPersona);
        listarPromocion();
    }

    public void listarPromocion(){
        requestQueue = Volley.newRequestQueue(this);
        String url = "http://192.168.1.7/api/promocion/listarPromocionGeneral/";
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                ListaPromocionPersonaAdapter adapter = new ListaPromocionPersonaAdapter(response);
                rvListarPromocion.setLayoutManager(new LinearLayoutManager(ListarPersonaPromocionActivity.this));
                rvListarPromocion.setAdapter(adapter);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(jsonArrayRequest);
    }

}
